import json
import jwt

def handler(event, context):
    # TODO implement
    auth = False
    jwtMessage = ''
    jwt_options = {
        'verify_signature': False,
        'verify_exp': False,
        'verify_nbf': False,
        'verify_iat': False,
        'verify_aud': False
    }

    print(event)
    headers = event.get('headers', {})
    authorization_header = next((value for key, value in headers.items() if key.lower() == 'authorization'), '')

    if not authorization_header or not authorization_header.startswith('Bearer '):
        jwtMessage = "Invalid Token"
    else:
        token = authorization_header[7:]
        try:
            jwtMessage = jwt.decode(token, algorithms=['HS256'], options=jwt_options)
            print(jwtMessage)
            auth = True
        except jwt.InvalidTokenError:
            jwtMessage = 'Invalid Token'

    response = {
        "isAuthorized": auth,
        "context": {
            "stringKey": "value",
            "jwtMessage": jwtMessage,
            "numberKey": 1,
            "booleanKey": auth,
            "arrayKey": ["value1", "value2"],
            "mapKey": {"value1": "value2"}
        }
    }

    return response